package T.HAUTIER;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView afficheur;
    private String sauveAfficheur;
    private String operationEnCours;
    private int etat = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        afficheur = (TextView) findViewById(R.id.textView);
    }

    public void touche0(View v){

        if(this.afficheur.getText().equals("Enter a number")) {
            this.afficheur.setText("");
        }

        this.afficheur.setText(afficheur.getText()+"0");
    }

    public void touche1(View v){

        if(this.afficheur.getText().equals("Enter a number")) {
            this.afficheur.setText("");
        }
        this.afficheur.setText(afficheur.getText()+"1");
    }

    public void plus (View v) {

        this.sauveAfficheur = afficheur.getText().toString();
        this.operationEnCours = "+";
        this.afficheur.setText("");
    }

    public void minus (View v) {

        this.sauveAfficheur = afficheur.getText().toString();
        this.operationEnCours = "-";
        this.afficheur.setText("");
    }

    public void toucheegal(View v){

        if(this.operationEnCours == "+") {
            int nombre1 = Integer.parseInt(this.sauveAfficheur, 2); // transformation en décimale
            int nombre2 = Integer.parseInt(this.afficheur.getText().toString(), 2); // transformation en décimale

            int calcul = nombre1 + nombre2;

            String result = Integer.toBinaryString(calcul);
            this.afficheur.setText(result.toString());
        }

        if(this.operationEnCours == "-") {
            int nombre1 = Integer.parseInt(this.sauveAfficheur, 2); // transformation en décimale
            int nombre2 = Integer.parseInt(this.afficheur.getText().toString(), 2); // transformation en décimale

            int calcul = nombre1 - nombre2;

            String result = Integer.toBinaryString(calcul);
            this.afficheur.setText(result.toString());
        }
    }

    public void toucheclear(View v){

        this.sauveAfficheur = "";
        this.afficheur.setText("Enter a number");
    }

}
